const authResolver = require('./users_actions');
const noteResolver = require('./note_actions');

const rootResolver = {
  ...authResolver,
  ...noteResolver
};

module.exports = rootResolver;
