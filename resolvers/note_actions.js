const Note = require('../models/note');
const jwt = require('jsonwebtoken');  
const bcrypt = require('bcryptjs');

let current_datetime = new Date()
let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds() 


module.exports = {
    createNote: async (args, req) => {
      if (req.errorCode !== 0) {
        return { 
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: req.errorCode,
            userId: req.userId
            }
          }       
      }

      if (args.header.length < 1 ) {
        return { 
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: -10,
            userId: req.userId
            }
          }   
      }

      if (args.body.length < 1 ) {
        return { 
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: -11,
            userId: req.userId
            }
          }   
      }

        try {
        const note = new Note({
          header: args.header,
          body: args.body,
          userFk: args.userFk,
          creationDate: formatted_date
        });

        const result = await note.save();
      
        return {
          _id: note._id,
          header: note.header,
          body: note.body,
          userFk: note.userFk,
          creationDate: note.creationDate,
          updateDate: null,
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: req.errorCode,
            userId: req.userId
          }
        }
          } catch (err) {
              return {
                authOutput: {
                  token: req.token,
                  tokenRefresh: req.tokenRefresh,
                  errorCode: -9,
                  userId: req.userId
                }
              }
            }
    },   
    notes: async (args, req) => {

      if (req.errorCode !== 0) {
        return [{ 
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: req.errorCode,
            userId: req.userId
            }
          }
        ]
      }

      try {
        const notes = await Note.find({ userFk: req.userId });

      return notes.map(note => {

          return {
            _id: note._id,
            header: note.header,
            body: note.body,
            userFk: note.userFk,
            creationDate: note.creationDate,
            updateDate: note.updateDate,
            authOutput: {
              token: req.token,
              tokenRefresh: req.tokenRefresh,
              errorCode: req.errorCode,
              userId: req.userId
            }
          }
       });
      } catch (err) {
          return [{ 
            authOutput: {
              token: req.token,
              tokenRefresh: req.tokenRefresh,
              errorCode: -5,
              userId: req.userId
              }
            }
          ]
      }
    },
    noteOne: async (args, req) => {   
      if (req.errorCode !== 0) {
        return { 
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: req.errorCode,
            userId: req.userId
            }
          }    
      }

      try {
        const note = await Note.findById(args._id);

          return {
            _id: note._id,
            header: note.header,
            body: note.body,
            userFk: note.userFk,
            creationDate: note.creationDate,
            updateDate: note.updateDate,
            authOutput: {
              token: req.token,
              tokenRefresh: req.tokenRefresh,
              errorCode: req.errorCode,
              userId: req.userId
            }
          }   
      } catch (err) {
          return { 
            authOutput: {
              token: req.token,
              tokenRefresh: req.tokenRefresh,
              errorCode: -12,
              userId: req.userId
              }
            }          
        }
    },
    updateNote: async (args, req) => {   
      if (req.errorCode !== 0) {
        return { 
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: req.errorCode,
            userId: req.userId
            }
          }    
      }
      try {
        let noteNew = await Note.findOneAndUpdate({ _id: args._id }, {header: args.header, body: args.body, updateDate: formatted_date}, {new: true});



        return {
          _id: noteNew._id,
          header: noteNew.header,
          body: noteNew.body,
          userFk: noteNew.userFk,
          creationDate: noteNew.creationDate,
          updateDate: noteNew.updateDate,
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: req.errorCode,
            userId: req.userId
          }
        }  
  
      } catch (err) {
        return { 
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: -13,
            userId: req.userId
            }
          }          
      }

    },
    deleteNote: async (args, req) => {   
      if (req.errorCode !== 0) {
        return { 
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: req.errorCode,
            userId: req.userId
            }
          }    
      }

      try {
        let noteDelete = await Note.findByIdAndRemove(args._id);

        return {
          _id: noteDelete._id,
          header: noteDelete.header,
          body: noteDelete.body,
          userFk: noteDelete.userFk,
          creationDate: noteDelete.creationDate,
          updateDate: noteDelete.updateDate,
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: req.errorCode,
            userId: req.userId
          }
        }  
  
      } catch (err) {
        return { 
          authOutput: {
            token: req.token,
            tokenRefresh: req.tokenRefresh,
            errorCode: -14,
            userId: req.userId
            }
          }          
      }
    }
};
  

