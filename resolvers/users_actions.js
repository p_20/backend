const User = require('../models/users');
const jwt = require('jsonwebtoken');  
const bcrypt = require('bcryptjs');


module.exports = {
  createUser: async args => {
    try {
      const existingUser = await User.findOne({ login: args.login });
      if (existingUser) {
        return { userId: existingUser._id, errorCode: -4 }
      }

      if (args.login.length < 8) {
        return { userId: null, errorCode: -8 }        
      }

      if (args.password.length < 8) {
        return { userId: null, errorCode: -6 }
      }

      if (args.password !== args.repassword) {
        return { userId: null, errorCode: -7 }
      }

      const hashedPassword = await bcrypt.hash(args.password, 12);

      const user = new User({
        login: args.login,
        password: hashedPassword,
        email: args.email
      });

      const result = await user.save();

      return { userId: user._id, errorCode: 0 }

    } catch (err) {
      return { userId: null, errorCode: -3 }
    }
  },
  login: async ({ login, password }) => {
    try {
      const user = await User.findOne({ login: login });
      if (!user) {
        return { userId: null, token: "", tokenRefresh: "", errorCode: -1 }
      }
      const isEqual = await bcrypt.compare(password, user.password);
      if (!isEqual) {
        return { userId: user.id, token: "", tokenRefresh: "", errorCode: -2 }
      }
      else {
        const token = jwt.sign({ userId: user.id, login: user.login }, process.env.SECRET, { expiresIn: '1m'});
        const tokenRefresh = jwt.sign({ userId: user.id, login: user.login }, process.env.SECRET_REFRESH, { expiresIn: '1000m'});
        return { userId: user.id, token: token, tokenRefresh: tokenRefresh, errorCode: 0 }
      } 
    } catch (err) {
      return { userId: user.id, token: "", tokenRefresh: "", errorCode: -3 }
    }  
  }

};

