const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const noteSchema = new Schema({
    header: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    userFk: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    creationDate: { 
        type: Date, 
        required: true
    },
    updateDate: { 
        type: Date, 
        required: false
    }


});


module.exports = mongoose.model('Note', noteSchema);

