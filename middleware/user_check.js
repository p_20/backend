const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    let decodedToken;
    let decodedTokenRefresh;
    const authHeader = req.get('Authorization');
    const authHeaderRefresh = req.get('AuthorizationRefresh');

    try {
        if (!authHeader || !authHeaderRefresh) {
            req.isAuth = false;
            req.userId = null;
            req.token = "";
            req.tokenRefresh = "";
            req.errorCode = -31;
        } 
        else {
            const token = authHeader.split(' ')[1]; 
            const tokenRefresh = authHeaderRefresh.split(' ')[1]; 
            if (!token || token === '' || !tokenRefresh || tokenRefresh === '') {
                req.isAuth = false;
                req.userId = null;
                req.token = "";
                req.tokenRefresh = "";
                req.errorCode = -32;
            }
            else {
                try { decodedToken = jwt.verify(token, process.env.SECRET); } catch (err) { }
                if (decodedToken) {                   
                    req.isAuth = true;
                    req.userId = decodedToken.userId;
                    req.token = token;
                    req.tokenRefresh = tokenRefresh;
                    req.errorCode = 0;
                }
                else {
                    try { decodedTokenRefresh = jwt.verify(tokenRefresh, process.env.SECRET_REFRESH); } catch (err) { }
                    if (decodedTokenRefresh) {                      
                        const tokenR = jwt.sign({ userId: decodedTokenRefresh.userId, login: decodedTokenRefresh.login }, process.env.SECRET, { expiresIn: '1m'});
                        const tokenRefreshR = jwt.sign({ userId: decodedTokenRefresh.userId, login: decodedTokenRefresh.login }, process.env.SECRET_REFRESH, { expiresIn: '1000m'});
                        req.isAuth = true;
                        req.userId = decodedTokenRefresh.userId;
                        req.token = tokenR;
                        req.tokenRefresh = tokenRefreshR;
                        req.errorCode = 0;
                    }
                    else {
                        req.isAuth = false;
                        req.userId = null;
                        req.token = "";
                        req.tokenRefresh = "";
                        req.errorCode = -34;
                    }
                }
            }
        }
    } catch (err) {
        req.isAuth = false;
        req.userId = null;
        req.token = "";
        req.tokenRefresh = "";
        req.errorCode = -35;
    }

    next();
};

