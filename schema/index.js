const graphql = require('graphql');
const graphql_iso_date = require('graphql-iso-date');

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull } = graphql;

const { GraphQLDateTime } = graphql_iso_date;

const UserType = new GraphQLObjectType({
    name: 'UserData',
    fields: ( ) => ({
        userId: { type: GraphQLID },
        login: { type: GraphQLString },
        password: { type: GraphQLString },
        errorCode: { type: GraphQLInt }
    })
});  

const AuthDataType = new GraphQLObjectType({
    name: 'AuthData',
    fields: ( ) => ({
        userId: { type: GraphQLID },
        token: { type: GraphQLString },
        tokenRefresh: { type: GraphQLString },
        errorCode: { type: GraphQLInt }
    })
});

const NoteType = new GraphQLObjectType({
    name: 'Note',
    fields: ( ) => ({
        _id: { type: GraphQLID },
        header: { type: GraphQLString },
        title: { type: GraphQLString },
        footer: { type: GraphQLString },       
        body: { type: GraphQLString },    
        userFk: { type: GraphQLID },  
        creationDate: { type: GraphQLDateTime },
        updateDate: { type: GraphQLDateTime },
        authOutput: { type: AuthDataType }
    })
});

const NoteInputType = new GraphQLObjectType({
  name: 'NoteInput',
  fields: ( ) => ({
      header: { type: GraphQLString },
      title: { type: GraphQLString },
      body: { type: GraphQLString },  
      footer: { type: GraphQLString },       
      userFk: { type: GraphQLID }
  })
});

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    notes: {
        type: new GraphQLList(NoteType),
    },
    noteOne: {
        type: NoteType,
        args: { 
            _id: { type: GraphQLID },
        },
    },
    login: {
        type: AuthDataType,
        args: { 
            login: { type: GraphQLString }, 
            password: { type: GraphQLString } 
            },
        }
    }
});

const RootMutation = new GraphQLObjectType({
  name: 'RootMutationType',
  fields: {
      createNote: {
          type: NoteType,
          args: { 
            header: { type: GraphQLString },
            title: { type: GraphQLString },
            body: { type: GraphQLString },
            footer: { type: GraphQLString },
            userFk: { type: GraphQLID },            
          },
      },
      createUser: {
          type: UserType,
          args: { 
            login: { type: GraphQLString },
            password: { type: GraphQLString },    
            repassword: { type: GraphQLString },
            email: { type: GraphQLString },
          },
      },
      updateNote: {
          type: NoteType,
          args: {
            header: { type: GraphQLString },
            body: { type: GraphQLString },
            _id: { type: GraphQLID },  
          }
      },
      deleteNote: {
          type: NoteType,
          args: {
            _id: { type: GraphQLID },  
          }
      }
  }
});



module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: RootMutation
});


